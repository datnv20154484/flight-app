package com.dji.sdk.sample.ui;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.dji.sdk.sample.JsonPlaceHolderApi;
import com.dji.sdk.sample.R;
import com.dji.sdk.sample.adapter.SpinDroneAdapter;
import com.dji.sdk.sample.adapter.SpinEPAdapter;
import com.dji.sdk.sample.model.Drone;
import com.dji.sdk.sample.model.ElectricPole;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddPhotoFragment extends Fragment {

    private final String SHARED_PREFERENCES_NAME = "login";
    private final String ID_USER = "id_user";
    private static final int GALLERY_REQUEST_CODE = 1234;
    private JsonPlaceHolderApi jsonPlaceHolderApi;
    private JsonPlaceHolderApi jsonPlaceHolderApi_photo;
    private List<ElectricPole> list_ep = new ArrayList<>();
    private List<Drone> list_drone = new ArrayList<>();
    private boolean crop = false;
    private MultipartBody.Part part;

    private Button mbtn_addphoto_choosefile;
    private Button mbtn_addphoto_save;
    private Button mbtn_addphoto_cancel;
    private EditText medt_addphoto_title;
    private EditText medt_addphoto_datecreaete;
    private EditText medt_addphoto_dateimport;
    private EditText medt_addphoto_des;
    private Spinner msp_addphoto_idpole;
    private Spinner msp_addphoto_iddrone;
    private RadioGroup mrg_addphoto;
    private RadioButton mrb_addphoto_true;
    private RadioButton mrb_addphoto_false;
    private ImageView mimv_addphoto_image;
    private TextView mtv_addphoto_status;

    public AddPhotoFragment() {
        // Required empty public constructor
    }

    private void pickFromGallery() {
        //Create an Intent with action as ACTION_PICK
        Intent intent = new Intent(Intent.ACTION_PICK);
        // Sets the type as image/*. This ensures only components of type image are selected
        intent.setType("image/*");
        //We pass an extra array with the accepted mime types. This will ensure only components with these MIME types as targeted.
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        // Launching the Intent
        startActivityForResult(intent, GALLERY_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        if (resultCode == Activity.RESULT_OK)
            switch (requestCode) {
                case GALLERY_REQUEST_CODE:
                    //data.getData return the content URI for the selected Image
                    Uri selectedImage = data.getData();
                    File file = new File(String.valueOf(selectedImage));
                    RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
                    // Create MultipartBody.Part using file request-body,file name and part name
                    part = MultipartBody.Part.createFormData("image", file.getName(), fileReqBody);
                    // Set the Image in ImageView after decoding the String
                    mimv_addphoto_image.setImageURI(selectedImage);
                    break;

            }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_add_photo, container, false);
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(SHARED_PREFERENCES_NAME, getContext().MODE_PRIVATE);
        String id_user = sharedPreferences.getString(ID_USER, "");
        medt_addphoto_title = root.findViewById(R.id.edt_addphoto_title);
        medt_addphoto_datecreaete = root.findViewById(R.id.edt_addphoto_datecreate);
        medt_addphoto_dateimport = root.findViewById(R.id.edt_addphoto_dateimport);
        medt_addphoto_des = root.findViewById(R.id.edt_addphoto_des);
        msp_addphoto_idpole = root.findViewById(R.id.sp_addphoto_idpole);
        msp_addphoto_iddrone = root.findViewById(R.id.sp_addphoto_iddrone);
        mrg_addphoto = root.findViewById(R.id.rg_addphoto);
        mrb_addphoto_true = root.findViewById(R.id.rb_addphoto_true);
        mrb_addphoto_false = root.findViewById(R.id.rb_addphoto_false);
        mbtn_addphoto_choosefile = root.findViewById(R.id.btn_addphoto_choosefile);
        mbtn_addphoto_save = root.findViewById(R.id.btn_addphoto_save);
        mbtn_addphoto_cancel = root.findViewById(R.id.btn_addphoto_cancel);
        mimv_addphoto_image = root.findViewById(R.id.imv_addphoto_image);
        mtv_addphoto_status = root.findViewById(R.id.tv_addphoto_status);

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.0.169:8081/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Retrofit retrofit_photo = new Retrofit.Builder()
                .baseUrl("http://192.168.0.169:8080/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        jsonPlaceHolderApi_photo = retrofit_photo.create(JsonPlaceHolderApi.class);

        Call<List<Drone>> getalldrone = jsonPlaceHolderApi.getallDrone();
        getalldrone.enqueue(new Callback<List<Drone>>() {
            @Override
            public void onResponse(Call<List<Drone>> call, Response<List<Drone>> response) {
                list_drone = response.body();
                SpinDroneAdapter spinDroneAdapter = new SpinDroneAdapter(getContext(), android.R.layout.simple_spinner_item, (ArrayList<Drone>) list_drone);
                spinDroneAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
                msp_addphoto_iddrone.setAdapter(spinDroneAdapter);
            }

            @Override
            public void onFailure(Call<List<Drone>> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });


        Call<List<ElectricPole>> getallep = jsonPlaceHolderApi.getAllEP();
        getallep.enqueue(new Callback<List<ElectricPole>>() {
            @Override
            public void onResponse(Call<List<ElectricPole>> call, Response<List<ElectricPole>> response) {
                list_ep = response.body();
                SpinEPAdapter spinEPAdapter = new SpinEPAdapter(getContext(), android.R.layout.simple_spinner_item, (ArrayList<ElectricPole>) list_ep);
                spinEPAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
                msp_addphoto_idpole.setAdapter(spinEPAdapter);

            }

            @Override
            public void onFailure(Call<List<ElectricPole>> call, Throwable t) {

            }
        });

        mrg_addphoto.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                int checkradio = radioGroup.getCheckedRadioButtonId();
                if (checkradio == R.id.rb_addphoto_true) {
                    crop = false;
                } else {
                    crop = true;
                }
            }
        });

        mrb_addphoto_true.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                crop = false;
            }
        });

        mrb_addphoto_false.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                crop = true;
            }
        });


        mbtn_addphoto_choosefile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Create the ACTION_GET_CONTENT Intent
                pickFromGallery();
            }
        });

        mbtn_addphoto_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft_rep = fm.beginTransaction();
                ft_rep.replace(R.id.nav_host_fragment, new DataFlightManaFragment());
                ft_rep.commit();
            }
        });

        mbtn_addphoto_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (medt_addphoto_title.getText().toString().equals("") ||
                        medt_addphoto_datecreaete.getText().toString().equals("") ||
                        medt_addphoto_dateimport.getText().toString().equals("") ||
                        medt_addphoto_des.getText().toString().equals("")) {
                    mtv_addphoto_status.setText("Bạn cần nhập đầy đủ thông tin Ảnh");
                } else {
                    new AlertDialog.Builder(getActivity())
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle("Lưu thông tin")
                            .setMessage("Bạn có muốn lưu thông tin không ?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    String title = medt_addphoto_title.getText().toString();
                                    String datecreate = medt_addphoto_datecreaete.getText().toString();
                                    String dateimport = medt_addphoto_dateimport.getText().toString();
                                    String des = medt_addphoto_des.getText().toString();
                                    String idpole = list_ep.get(msp_addphoto_idpole.getSelectedItemPosition()).get_id();
                                    String iddrone = list_drone.get(msp_addphoto_iddrone.getSelectedItemPosition()).get_id();

                                    Call<String> addPhoto = jsonPlaceHolderApi_photo.addphoto(title, datecreate, dateimport, des, idpole, id_user, iddrone, crop, part);
                                    addPhoto.enqueue(new Callback<String>() {
                                        @Override
                                        public void onResponse(Call<String> call, Response<String> response) {
                                            System.out.println(response.body());
                                        }

                                        @Override
                                        public void onFailure(Call<String> call, Throwable t) {

                                        }
                                    });

                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            })
                            .show();
                }
            }
        });


        return root;
    }
}
