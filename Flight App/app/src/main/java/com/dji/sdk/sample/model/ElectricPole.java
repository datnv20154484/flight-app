package com.dji.sdk.sample.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class ElectricPole {

    @SerializedName("_id")
    private String _id;
    @SerializedName("pole_Name")
    private String pole_Name;
    @SerializedName("pole_Latitude")
    private Double pole_Latitude;
    @SerializedName("pole_Longitude")
    private Double pole_Longitude;
    @SerializedName("description")
    private String description;
    @SerializedName("buildTime")
    private String buildTime;
    @SerializedName("ep_MaintenanceTime")
    private String ep_MaintenanceTime;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getPole_Name() {
        return pole_Name;
    }

    public void setPole_Name(String pole_Name) {
        this.pole_Name = pole_Name;
    }

    public Double getPole_Latitude() {
        return pole_Latitude;
    }

    public void setPole_Latitude(Double pole_Latitude) {
        this.pole_Latitude = pole_Latitude;
    }

    public Double getPole_Longitude() {
        return pole_Longitude;
    }

    public void setPole_Longitude(Double pole_Longitude) {
        this.pole_Longitude = pole_Longitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBuildTime() {
        return buildTime;
    }

    public void setBuildTime(String buildTime) {
        this.buildTime = buildTime;
    }

    public String getEp_MaintenanceTime() {
        return ep_MaintenanceTime;
    }

    public void setEp_MaintenanceTime(String ep_MaintenanceTime) {
        this.ep_MaintenanceTime = ep_MaintenanceTime;
    }

    public ElectricPole(String pole_Name, Double pole_Latitude, Double pole_Longitude, String description, String buildTime, String ep_MaintenanceTime) {
        this.pole_Name = pole_Name;
        this.pole_Latitude = pole_Latitude;
        this.pole_Longitude = pole_Longitude;
        this.description = description;
        this.buildTime = buildTime;
        this.ep_MaintenanceTime = ep_MaintenanceTime;
    }

    @Override
    public String toString() {
        return "ElectricPole{" +
                "_id='" + _id + '\'' +
                ", pole_Name='" + pole_Name + '\'' +
                ", pole_Latitude=" + pole_Latitude +
                ", pole_Longitude=" + pole_Longitude +
                ", description='" + description + '\'' +
                ", buildTime=" + buildTime +
                ", ep_MaintenanceTime=" + ep_MaintenanceTime +
                '}';
    }
}
