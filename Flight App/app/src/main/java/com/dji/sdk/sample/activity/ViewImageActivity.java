package com.dji.sdk.sample.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.dji.sdk.sample.R;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

public class ViewImageActivity extends AppCompatActivity {

    private PhotoView photoView;
    private Button mbtn_addinci;
    private Button mbtn_gohome;
    private  String idimage,idpole;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_image);
        photoView = findViewById(R.id.view_image_checkinci);
        mbtn_addinci = findViewById(R.id.btn_viewimage_addinci);
        mbtn_gohome = findViewById(R.id.btn_viewimage_gohome);

        Intent get = getIntent();
        idimage = get.getStringExtra("idimage");
        idpole = get.getStringExtra("idpole");
        String imageUrl = "http://192.168.0.169:8080/photos/byte/" + idimage;
        Picasso.get().load(imageUrl).into(photoView);
        mbtn_gohome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        mbtn_addinci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),AddIncidentActivity.class);
                intent.putExtra("idimage",idimage);
                intent.putExtra("idpole",idpole);
                startActivity(intent);
            }
        });

    }
}
