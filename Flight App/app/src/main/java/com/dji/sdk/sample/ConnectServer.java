package com.dji.sdk.sample;

import android.util.Log;

import com.dji.sdk.sample.adapter.EPAdapter;
import com.dji.sdk.sample.model.ElectricPole;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ConnectServer {

    public JsonPlaceHolderApi jsonPlaceHolderApi;
    public List<ElectricPole> list_elecpole = new ArrayList<>();
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");


    OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .build();
    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://192.168.0.169:8081/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build();

    public void Update_EP_Server(String id, ElectricPole electricPole){
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<String> call = jsonPlaceHolderApi.updateEP(id, electricPole);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
//                Log.d("Update EP", response.body());
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
//                Log.e("Update EP", "Lỗi: " + t.getMessage());
            }
        });

    }
}
