package com.dji.sdk.sample.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.dji.sdk.sample.JsonPlaceHolderApi;
import com.dji.sdk.sample.R;
import com.dji.sdk.sample.model.Employee;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UserEditActivity extends AppCompatActivity {

    private JsonPlaceHolderApi jsonPlaceHolderApi;
    private EditText medt_useredit_name;
    private EditText medt_useredit_sex;
    private EditText medt_useredit_birth;
    private EditText medt_useredit_nationid;
    private EditText medt_useredit_address;
    private EditText medt_useredit_mail;
    private EditText medt_useredit_phone;
    private Button mbtn_useredit_edit;
    private Button mbtn_useredit_done;
    private Button mbtn_useredit_save;
    private Button mbtn_useredit_cancel;
    private Button mbtn_useredit_delete;
    private TextView mtv_useredit_status;
    private Employee employee;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_edit);

        Intent intent = getIntent();
        String id = intent.getStringExtra("id");

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.0.169:8081/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        medt_useredit_name = findViewById(R.id.edt_useredit_name);
        medt_useredit_sex = findViewById(R.id.edt_useredit_sex);
        medt_useredit_birth = findViewById(R.id.edt_useredit_birth);
        medt_useredit_nationid = findViewById(R.id.edt_useredit_nationid);
        medt_useredit_address = findViewById(R.id.edt_useredit_address);
        medt_useredit_phone = findViewById(R.id.edt_useredit_phone);
        medt_useredit_mail = findViewById(R.id.edt_useredit_mail);
        mbtn_useredit_edit = findViewById(R.id.btn_useredit_edit);
        mbtn_useredit_save = findViewById(R.id.btn_useredit_save);
        mbtn_useredit_cancel = findViewById(R.id.btn_useredit_cancel);
        mbtn_useredit_done = findViewById(R.id.btn_useredit_done);
        mtv_useredit_status = findViewById(R.id.tv_useredit_status);
        mbtn_useredit_save.setVisibility(View.INVISIBLE);
        mbtn_useredit_cancel.setVisibility(View.INVISIBLE);
        mbtn_useredit_delete = findViewById(R.id.btn_useredit_delete);

        AlertDialog.Builder show = new AlertDialog.Builder(this);

        mbtn_useredit_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Call<String> call = jsonPlaceHolderApi.deleteUser(id);
                call.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
//                        Log.d("Delete User", response.body());
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
//                        Log.e("Delete User", t.getMessage());
                    }
                });
                Toast.makeText(getApplicationContext(),"Xóa Nhân viên Thành công",Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(),StartAppActivity.class);
                startActivity(i);
            }
        });

        // Lấy thông tin người dùng
        Call<Employee> call = jsonPlaceHolderApi.getEmpbyID(id);
        call.enqueue(new Callback<Employee>() {
            @Override
            public void onResponse(Call<Employee> call, Response<Employee> response) {
                employee = response.body();
                medt_useredit_name.setText(employee.getName());
                medt_useredit_address.setText(employee.getAddress());
                medt_useredit_sex.setText(employee.getSex());
                medt_useredit_birth.setText(employee.getBirth());
                medt_useredit_nationid.setText(employee.getNationid());
                medt_useredit_phone.setText(employee.getPhone());
                medt_useredit_mail.setText(employee.getMail());
            }

            @Override
            public void onFailure(Call<Employee> call, Throwable t) {
                Log.e("User Manager", "Lỗi: " + t.getMessage());
            }
        });

        mbtn_useredit_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        mbtn_useredit_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mbtn_useredit_edit.setVisibility(View.INVISIBLE);
                mbtn_useredit_done.setVisibility(View.INVISIBLE);
                mbtn_useredit_save.setVisibility(View.VISIBLE);
                mbtn_useredit_cancel.setVisibility(View.VISIBLE);
                medt_useredit_name.setEnabled(true);
                medt_useredit_sex.setEnabled(true);
                medt_useredit_birth.setEnabled(true);
                medt_useredit_nationid.setEnabled(true);
                medt_useredit_address.setEnabled(true);
                medt_useredit_phone.setEnabled(true);
                medt_useredit_mail.setEnabled(true);
            }
        });

        mbtn_useredit_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                medt_useredit_name.setText(employee.getName());
                medt_useredit_address.setText(employee.getAddress());
                medt_useredit_sex.setText(employee.getSex());
                medt_useredit_birth.setText(employee.getBirth());
                medt_useredit_nationid.setText(employee.getNationid());
                medt_useredit_phone.setText(employee.getPhone());
                medt_useredit_mail.setText(employee.getMail());
                medt_useredit_name.setEnabled(false);
                medt_useredit_sex.setEnabled(false);
                medt_useredit_birth.setEnabled(false);
                medt_useredit_address.setEnabled(false);
                medt_useredit_nationid.setEnabled(false);
                medt_useredit_phone.setEnabled(false);
                medt_useredit_mail.setEnabled(false);
                mbtn_useredit_cancel.setVisibility(View.INVISIBLE);
                mbtn_useredit_save.setVisibility(View.INVISIBLE);
                mbtn_useredit_edit.setVisibility(View.VISIBLE);
                mbtn_useredit_done.setVisibility(View.VISIBLE);
            }
        });

        mbtn_useredit_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (medt_useredit_name.getText().toString().equals("") ||
                        medt_useredit_sex.getText().toString().equals("") ||
                        medt_useredit_birth.getText().toString().equals("") ||
                        medt_useredit_address.getText().toString().equals("") ||
                        medt_useredit_nationid.getText().toString().equals("") ||
                        medt_useredit_phone.getText().toString().equals("") ||
                        medt_useredit_mail.getText().toString().equals("")) {
                    mtv_useredit_status.setText("Bạn cần nhập đầy đủ thông tin");
                } else {
                            show.setIcon(android.R.drawable.ic_dialog_alert);
                    show.setTitle("Lưu thông tin");
                    show.setMessage("Bạn có muốn lưu thông tin không ?");
                    show.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    employee.setName(medt_useredit_name.getText().toString());
                                    employee.setSex(medt_useredit_sex.getText().toString());
                                    employee.setBirth(medt_useredit_birth.getText().toString());
                                    employee.setAddress(medt_useredit_address.getText().toString());
                                    employee.setNationid(medt_useredit_nationid.getText().toString());
                                    employee.setMail(medt_useredit_mail.getText().toString());
                                    employee.setPhone(medt_useredit_phone.getText().toString());

                                    Call<String> call = jsonPlaceHolderApi.updateUser(id, employee);
                                    call.enqueue(new Callback<String>() {
                                        @Override
                                        public void onResponse(Call<String> call, Response<String> response) {
//                                            Log.d("Update User", response.body());
                                        }

                                        @Override
                                        public void onFailure(Call<String> call, Throwable t) {
//                                            Log.e("Update User", "Lỗi: " + t.getMessage());
                                        }
                                    });
                                    mtv_useredit_status.setText("Sửa thông tin người dùng thành công");
                                    mbtn_useredit_cancel.setVisibility(View.INVISIBLE);
                                    mbtn_useredit_save.setVisibility(View.INVISIBLE);
                                    mbtn_useredit_edit.setVisibility(View.VISIBLE);
                                    mbtn_useredit_done.setVisibility(View.VISIBLE);
                                    medt_useredit_name.setEnabled(false);
                                    medt_useredit_sex.setEnabled(false);
                                    medt_useredit_birth.setEnabled(false);
                                    medt_useredit_address.setEnabled(false);
                                    medt_useredit_nationid.setEnabled(false);
                                    medt_useredit_phone.setEnabled(false);
                                    medt_useredit_mail.setEnabled(false);
                                }
                            });
                    show.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    medt_useredit_name.setText(employee.getName());
                                    medt_useredit_address.setText(employee.getAddress());
                                    medt_useredit_sex.setText(employee.getSex());
                                    medt_useredit_birth.setText(employee.getBirth());
                                    medt_useredit_nationid.setText(employee.getNationid());
                                    medt_useredit_phone.setText(employee.getPhone());
                                    medt_useredit_mail.setText(employee.getMail());
                                    mbtn_useredit_cancel.setVisibility(View.INVISIBLE);
                                    mbtn_useredit_save.setVisibility(View.INVISIBLE);
                                    mbtn_useredit_edit.setVisibility(View.VISIBLE);
                                    mbtn_useredit_done.setVisibility(View.VISIBLE);
                                    medt_useredit_name.setEnabled(false);
                                    medt_useredit_sex.setEnabled(false);
                                    medt_useredit_birth.setEnabled(false);
                                    medt_useredit_address.setEnabled(false);
                                    medt_useredit_nationid.setEnabled(false);
                                    medt_useredit_phone.setEnabled(false);
                                    medt_useredit_mail.setEnabled(false);
                                }
                            });
                    show.show();
                }

            }
        });
    }
}
