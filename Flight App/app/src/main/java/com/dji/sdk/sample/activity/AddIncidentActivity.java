package com.dji.sdk.sample.activity;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.dji.sdk.sample.JsonPlaceHolderApi;
import com.dji.sdk.sample.R;
import com.dji.sdk.sample.adapter.SpinFixerAdapter;
import com.dji.sdk.sample.model.Employee;
import com.dji.sdk.sample.model.Incident;
import com.dji.sdk.sample.ui.CheckInciFragment;
import com.dji.sdk.sample.ui.IncidentFragment;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AddIncidentActivity extends AppCompatActivity {

    private final String SHARED_PREFERENCES_NAME = "login";
    private final String ID_USER = "id_user";
    private JsonPlaceHolderApi jsonPlaceHolderApi_Data;
    private JsonPlaceHolderApi jsonPlaceHolderApi_Inci;
    private String idimage;
    private String idpole;
    private String id_user;
    private String idfixer;
    private List<Employee> listfixer = new ArrayList<>();

    private PhotoView mpv_addinci_image;
    private EditText medt_addinci_name;
    private EditText medt_addinci_des;
    private EditText medt_addinci_date;
    private EditText medt_addinci_level;
    private Spinner msp_addinci_idfix;
    private Button mbtn_addinci_save;
    private Button mbtn_addinci_cancel;
    private TextView mtv_addinci_addStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_incident);
        Intent get = getIntent();
        idimage = get.getStringExtra("idimage");
        idpole = get.getStringExtra("idpole");
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFERENCES_NAME, this.MODE_PRIVATE);
        id_user = sharedPreferences.getString(ID_USER, "");

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.0.169:8081/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        jsonPlaceHolderApi_Data = retrofit.create(JsonPlaceHolderApi.class);

        Retrofit retrofit2 = new Retrofit.Builder()
                .baseUrl("http://192.168.0.169:8082/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        jsonPlaceHolderApi_Inci = retrofit2.create(JsonPlaceHolderApi.class);

        mpv_addinci_image = findViewById(R.id.photoview_addinci_image);
        medt_addinci_name = findViewById(R.id.edt_addinci_name);
        medt_addinci_des = findViewById(R.id.edt_addinci_des);
        medt_addinci_date = findViewById(R.id.edt_addinci_date);
        medt_addinci_level = findViewById(R.id.edt_addinci_level);
        msp_addinci_idfix = findViewById(R.id.sp_addinci_idfix);
        mbtn_addinci_save = findViewById(R.id.btn_addinci_save);
        mbtn_addinci_cancel = findViewById(R.id.btn_addinci_cancel);
        mtv_addinci_addStatus = findViewById(R.id.tv_addinci_addstatus);

        String imageUrl = "http://192.168.0.169:8080/photos/byte/" + idimage;
        Picasso.get().load(imageUrl).into(mpv_addinci_image);

        Call<List<Employee>> call = jsonPlaceHolderApi_Data.getEmpbyRole("fixer");
        call.enqueue(new Callback<List<Employee>>() {
            @Override
            public void onResponse(Call<List<Employee>> call, Response<List<Employee>> response) {
                listfixer = response.body();
                SpinFixerAdapter adapter = new SpinFixerAdapter(getApplicationContext(), R.layout.sp_item_addinci_fixxer, listfixer);
                adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
                msp_addinci_idfix.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<Employee>> call, Throwable t) {

            }
        });

        msp_addinci_idfix.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                idfixer = listfixer.get(i).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mbtn_addinci_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mbtn_addinci_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (medt_addinci_name.getText().toString().equals("") ||
                        medt_addinci_des.getText().toString().equals("") ||
                        medt_addinci_date.getText().toString().equals("") ||
                        medt_addinci_level.getText().toString().equals("")) {
                    mtv_addinci_addStatus.setText("Bạn cần nhập đầy đủ thông tin");
                } else {
                    Incident incident = new Incident();
                    incident.setName(medt_addinci_name.getText().toString());
                    incident.setDate(medt_addinci_date.getText().toString());
                    incident.setDes(medt_addinci_des.getText().toString());
                    incident.setLevel(medt_addinci_level.getText().toString());
                    incident.setStatus("norepair");
                    incident.setIdfix(idfixer);
                    incident.setImage(idimage);
                    incident.setIdpole(idpole);
                    incident.setIddetect(id_user);

                    Call<String> calladd = jsonPlaceHolderApi_Inci.addInci(incident);
                    calladd.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {

                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
//                                mtv_addinci_addStatus.setText(t.getMessage());
                        }
                    });
                    Intent i = new Intent(getApplicationContext(), StartAppActivity.class);
                    startActivity(i);
                }
            }
        });

    }
}
