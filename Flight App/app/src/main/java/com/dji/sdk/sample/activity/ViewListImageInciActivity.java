package com.dji.sdk.sample.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.dji.sdk.sample.JsonPlaceHolderApi;
import com.dji.sdk.sample.R;
import com.dji.sdk.sample.adapter.List_ImageCheckInciAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ViewListImageInciActivity extends AppCompatActivity {
    private JsonPlaceHolderApi jsonPlaceHolderApi;
    private List<String> listid = new ArrayList<>();

    private ListView mlv_imagecheckinci;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_image_inci);
        Intent i = getIntent();
        String idpole = i.getStringExtra("idpole");
        String date = i.getStringExtra("date");
        boolean crop = i.getBooleanExtra("crop",false);

        mlv_imagecheckinci = findViewById(R.id.lv_viewcheckinci);

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.0.169:8080/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Call<List<String>> call = jsonPlaceHolderApi.listimagecheck(idpole,date,crop);
        call.enqueue(new Callback<List<String>>() {
            @Override
            public void onResponse(Call<List<String>> call, Response<List<String>> response) {
                listid = response.body();
                List_ImageCheckInciAdapter adapter = new List_ImageCheckInciAdapter(getApplication(), R.layout.item_image_checkinci, (ArrayList<String>) listid);
                mlv_imagecheckinci.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<String>> call, Throwable t) {

            }
        });

        mlv_imagecheckinci.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                String idimage = listid.get(i);
                Intent intent = new Intent(getApplicationContext(), ViewImageActivity.class);
                intent.putExtra("idimage",idimage);
                intent.putExtra("idpole",idpole);
                startActivity(intent);
                return true;
            }
        });
    }
}
