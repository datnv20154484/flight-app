package com.dji.sdk.sample;

import com.dji.sdk.sample.model.Drone;
import com.dji.sdk.sample.model.ElectricPole;
import com.dji.sdk.sample.model.Employee;
import com.dji.sdk.sample.model.Incident;
import com.dji.sdk.sample.model.Photo;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface JsonPlaceHolderApi {

    @GET("employees/")
    Call<List<Employee>> getAllEmp();

    @GET("employees/login/")
    Call<Employee> checkLogin(@Query("username") String username, @Query("password") String password);

    @GET("employees/{id}")
    Call<Employee> getEmpbyID(@Path("id") String id);

    @GET("employees/username/{username}")
    Call<Employee> getEmpbyUsername(@Path("username") String username);

    @GET("employees/mail/{mail}")
    Call<Employee> getEmpbyMail(@Path("mail") String mail);

    @GET("employees/phone/{phone}")
    Call<Employee> getEmpbyPhone(@Path("phone") String phone);

    @GET("employees/send")
    Call<String> sendMail(@Query("phone") String phone);

    @PUT("employees/{id}")
    Call<String> updateUser(@Path("id") String id, @Body Employee employee);

    @DELETE("employees/{id}")
    Call<String> deleteUser(@Path("id") String id);

    @POST("employees/")
    Call<String> addUser(@Body Employee employee);

    @GET("employees/role/{role}")
    Call<List<Employee>> getEmpbyRole(@Path("role") String role);

    @GET("electricpoles/")
    Call<List<ElectricPole>> getAllEP();

    @GET("electricpoles/{id}")
    Call<ElectricPole> getEPbyID(@Path("id") String id);

    @DELETE("electricpoles/{id}")
    Call<String> deleteEP(@Path("id") String id);

    @PUT("electricpoles/{id}")
    Call<String> updateEP(@Path("id") String id, @Body ElectricPole electricPole);

    @GET("drones/")
    Call<List<Drone>> getallDrone();

    @GET("drones/{id}")
    Call<Drone> getDrone(@Path("id") String id);

    @GET("photos/{id}")
    Call<Photo> getphotoBYId(@Path("id") String id);

    @GET("photos/byte/{id}")
    Call<byte[]> getphotobyte(@Path("id") String id);

    @GET("photos/countall")
    Call<Integer> countphoto();

    @GET("videos/countall")
    Call<Integer> countvideo();

    @GET("photos/listimagecheck")
    Call<List<String>> listimagecheck(@Query("idpole") String idpole,@Query("date") String date,@Query("crop") boolean crop);

    @POST("incidents")
    Call<String> addInci(@Body Incident incident);

    @GET("incidents")
    Call<List<Incident>> getallinci();

    @GET("incidents/{id}")
    Call<Incident> getincibyid(@Path("id") String id);

    @PUT("incidents/{id}")
    Call<String> modiffyincident(@Path("id") String id, @Body Incident incident);

    @POST("photos/add")
    Call<String> addphoto(@Query("title") String title, @Query("dateCreate") String dateCreate,
                          @Query("dateImport") String dateImport, @Query("des") String des,
                          @Query("idpole") String idpole, @Query("iduser") String iduser,
                          @Query("iddrone") String iddrone, @Query("crop") boolean crop, @Part("image") MultipartBody.Part file);
}
