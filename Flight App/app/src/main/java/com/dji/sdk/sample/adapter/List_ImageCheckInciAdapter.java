package com.dji.sdk.sample.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.dji.sdk.sample.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class List_ImageCheckInciAdapter extends BaseAdapter {
    private Context context;
    private int layout;
    private ArrayList<String> listsp;

    public List_ImageCheckInciAdapter(Context context, int layout, ArrayList<String> listsp) {
        this.context = context;
        this.layout = layout;
        this.listsp = listsp;
    }

    @Override
    public int getCount() {
        return listsp.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {

        ImageView imageView;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        List_ImageCheckInciAdapter.ViewHolder holder;
        if (view == null) {
            holder = new List_ImageCheckInciAdapter.ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(layout, null);
            holder.imageView = view.findViewById(R.id.image_checiinci);
            view.setTag(holder);

        } else {
            holder = (List_ImageCheckInciAdapter.ViewHolder) view.getTag();
        }
        String sp = listsp.get(i);
        String imageUrl = "http://192.168.0.169:8080/photos/byte/" + sp;
        Picasso.get().load(imageUrl).into(holder.imageView);
        return view;

    }
}
