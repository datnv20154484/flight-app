package com.dji.sdk.sample.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.dji.sdk.sample.JsonPlaceHolderApi;
import com.dji.sdk.sample.R;
import com.dji.sdk.sample.activity.UserEditActivity;
import com.dji.sdk.sample.adapter.UserManagerListAdapter;
import com.dji.sdk.sample.model.Employee;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UserManaFragment extends Fragment {

    private TextView mtv_sum_emp;
    private TextView mtv_pilot_emp;
    private TextView mtv_fix_emp;
    private TextView mtv_mana_empl;
    private ListView mlist_emp;
    private Button mbtn_add_user;
    private int sum = 0, sum_pilot = 0, sum_mana = 0, sum_fix = 0;

    private JsonPlaceHolderApi jsonPlaceHolderApi;
    private ArrayList<Employee> list_employee = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root =  inflater.inflate(R.layout.fragment_user_mana, container, false);
        // Inflate the layout for this fragment
        mtv_sum_emp = root.findViewById(R.id.tv_sum_emp);
        mtv_mana_empl = root.findViewById(R.id.tv_sys_emp);
        mtv_fix_emp = root.findViewById(R.id.tv_emp_fix);
        mtv_pilot_emp = root.findViewById(R.id.tv_pilot_emp);
        mbtn_add_user = root.findViewById(R.id.btn_user_manager_add);
        mlist_emp = root.findViewById(R.id.lv_emp);

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.0.169:8081/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);


        mbtn_add_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft_rep = fm.beginTransaction();
                ft_rep.replace(R.id.nav_host_fragment, new AddUserFragment());
                ft_rep.commit();
            }
        });

        //Lấy thông tin của tất cả người dùng
        Call<List<Employee>> call = jsonPlaceHolderApi.getAllEmp();
        call.enqueue(new Callback<List<Employee>>() {
            @Override
            public void onResponse(Call<List<Employee>> call, Response<List<Employee>> response) {
                List<Employee> list = response.body();

                for (Employee emp : list) {
//                    Log.d("User", emp.toString());
                    if (emp.getRole().equals("pilot")) {
                        sum_pilot++;
                        list_employee.add(emp);
                    }
                    if (emp.getRole().equals("admin")) {
                        sum++;
                    }
                    if (emp.getRole().equals("manager")) {
                        sum_mana++;
                        list_employee.add(emp);
                    }
                    if (emp.getRole().equals("fixer")) {
                        sum_fix++;
                        list_employee.add(emp);
                    }
                }
                mtv_sum_emp.setText(Integer.toString((list.size() - sum)));
                mtv_pilot_emp.setText(Integer.toString(sum_pilot));
                mtv_mana_empl.setText(Integer.toString(sum_mana));
                mtv_fix_emp.setText(Integer.toString(sum_fix));
                UserManagerListAdapter adapter = new UserManagerListAdapter(getActivity(), R.layout.list_item_employee, list_employee);
                mlist_emp.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<Employee>> call, Throwable t) {
                Log.e("User Manager", "Lỗi: " + t.getMessage());
            }
        });

        mlist_emp.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Employee e = list_employee.get(i);
                Intent intent = new Intent(getContext(), UserEditActivity.class);
                intent.putExtra("id", e.getId());
                startActivity(intent);

            }
        });

        mtv_mana_empl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<Employee> list_mana_emp = new ArrayList<>();
                for (Employee emp : list_employee) {
                    if (emp.getRole().equals("manager")) {
                        list_mana_emp.add(emp);
                    }
                }
                UserManagerListAdapter adapter = new UserManagerListAdapter(getActivity(), R.layout.list_item_employee, list_mana_emp);
                mlist_emp.setAdapter(adapter);
            }
        });
        mtv_fix_emp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<Employee> list_fix_emp = new ArrayList<>();
                for (Employee emp : list_employee) {
                    if (emp.getRole().equals("fixer")) {
                        list_fix_emp.add(emp);
                    }
                }
                UserManagerListAdapter adapter = new UserManagerListAdapter(getActivity(), R.layout.list_item_employee, list_fix_emp);
                mlist_emp.setAdapter(adapter);
            }
        });
        mtv_pilot_emp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<Employee> list_pilot_emp = new ArrayList<>();
                for (Employee emp : list_employee) {
                    if (emp.getRole().equals("pilot")) {
                        list_pilot_emp.add(emp);
                    }
                }
                UserManagerListAdapter adapter = new UserManagerListAdapter(getActivity(), R.layout.list_item_employee, list_pilot_emp);
                mlist_emp.setAdapter(adapter);
            }
        });

        mtv_sum_emp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserManagerListAdapter adapter = new UserManagerListAdapter(getActivity(), R.layout.list_item_employee, list_employee);
                mlist_emp.setAdapter(adapter);
            }
        });
        return root;
    }


}
