package com.dji.sdk.sample.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.dji.sdk.sample.JsonPlaceHolderApi;
import com.dji.sdk.sample.R;
import com.dji.sdk.sample.adapter.SpinFixerAdapter;
import com.dji.sdk.sample.model.ElectricPole;
import com.dji.sdk.sample.model.Employee;
import com.dji.sdk.sample.model.Incident;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DetailInciActivity extends AppCompatActivity {

    private JsonPlaceHolderApi jsonPlaceHolderApi;
    private JsonPlaceHolderApi jsonPlaceHolderApi_Data;
    private List<Employee> listfixer = new ArrayList<>();
    private Incident incident;

    private PhotoView mpv_detailinci_image;
    private EditText medt_detailinci_name;
    private EditText medt_detailinci_des;
    private EditText medt_detailinci_date;
    private EditText medt_detailinci_level;
    private EditText medt_detailinci_status;
    private EditText medt_detailinci_idpole;
    private EditText medt_detailinci_iddetect;
    private EditText medt_detailinci_idfixer;
    private Spinner msp_detailinci_level;
    private Spinner msp_detailinci_status;
    private Spinner msp_detailinci_idfixer;
    private Button mbtn_detailinci_map;
    private Button mbtn_detailinci_done;
    private Button mbtn_detailinci_edit;
    private Button mbtn_detailinci_save;
    private Button mbtn_detailinci_cancel;
    private TextView mtv_detailinci_statusedit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_inci);

        Intent get = getIntent();
        String idinci = get.getStringExtra("idinci");

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.0.169:8081/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        jsonPlaceHolderApi_Data = retrofit.create(JsonPlaceHolderApi.class);

        Retrofit retrofit1 = new Retrofit.Builder()
                .baseUrl("http://192.168.0.169:8082/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        jsonPlaceHolderApi = retrofit1.create(JsonPlaceHolderApi.class);

        AlertDialog.Builder show = new AlertDialog.Builder(this);

        mpv_detailinci_image = findViewById(R.id.pv_detailinci_image);
        medt_detailinci_name = findViewById(R.id.edt_detailinci_name);
        medt_detailinci_des = findViewById(R.id.edt_detailinci_des);
        medt_detailinci_date = findViewById(R.id.edt_detailinci_date);
        medt_detailinci_level = findViewById(R.id.edt_detailinci_level);
        medt_detailinci_status = findViewById(R.id.edt_detailinci_status);
        medt_detailinci_idpole = findViewById(R.id.edt_detailinci_idpole);
        medt_detailinci_iddetect = findViewById(R.id.edt_detailinci_iddetect);
        medt_detailinci_idfixer = findViewById(R.id.edt_detailinci_idfixer);
        msp_detailinci_status = findViewById(R.id.sp_detailinci_status);
        msp_detailinci_level = findViewById(R.id.sp_detailinci_level);
        msp_detailinci_idfixer = findViewById(R.id.sp_detailinci_idfixer);
        mbtn_detailinci_map = findViewById(R.id.btn_detailinci_map);
        mbtn_detailinci_done = findViewById(R.id.btn_detailinci_done);
        mbtn_detailinci_edit = findViewById(R.id.btn_detailinci_edit);
        mbtn_detailinci_save = findViewById(R.id.btn_detailinci_save);
        mbtn_detailinci_cancel = findViewById(R.id.btn_detailinci_cancel);


        String level[] = {
                "Mức độ nguy hiểm 1",
                "Mức độ nguy hiểm 2",
                "Mức độ nguy hiểm 3",
                "Mức độ nguy hiểm 4"};
        ArrayAdapter<String> adapterlevel = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, level);
        adapterlevel.setDropDownViewResource
                (android.R.layout.simple_list_item_single_choice);
        //Thiết lập adapter cho Spinner
        msp_detailinci_level.setAdapter(adapterlevel);

        String status[] = {
                "Chưa sửa chữa",
                "Đang sửa chữa",
                "Đã sửa chữa",
                "Chờ xác nhận"};
        ArrayAdapter<String> adapterstatus = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, status);
        adapterstatus.setDropDownViewResource
                (android.R.layout.simple_list_item_single_choice);
        //Thiết lập adapter cho Spinner
        msp_detailinci_status.setAdapter(adapterstatus);

        Call<List<Employee>> call = jsonPlaceHolderApi_Data.getEmpbyRole("fixer");
        call.enqueue(new Callback<List<Employee>>() {
            @Override
            public void onResponse(Call<List<Employee>> call, Response<List<Employee>> response) {
                listfixer = response.body();
                SpinFixerAdapter adapter = new SpinFixerAdapter(getApplicationContext(), R.layout.sp_item_addinci_fixxer, listfixer);
                adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
                msp_detailinci_idfixer.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<Employee>> call, Throwable t) {

            }
        });

        Call<Incident> getbyid = jsonPlaceHolderApi.getincibyid(idinci);
        getbyid.enqueue(new Callback<Incident>() {
            @Override
            public void onResponse(Call<Incident> call, Response<Incident> response) {
                incident = response.body();
                String imageUrl = "http://192.168.0.169:8080/photos/byte/" + incident.getImage();
                Picasso.get().load(imageUrl).into(mpv_detailinci_image);
                medt_detailinci_name.setText(incident.getName());
                medt_detailinci_des.setText(incident.getDes());
                medt_detailinci_date.setText(incident.getDate());
                medt_detailinci_level.setText(incident.getLevel());
                if (incident.getStatus().equals("norepair")) {
                    medt_detailinci_status.setText("Chưa sửa chữa");
                }
                if (incident.getStatus().equals("repairing")) {
                    medt_detailinci_status.setText("Đang sửa chữa");
                }
                if (incident.getStatus().equals("repaired")) {
                    medt_detailinci_status.setText("Đã sửa chữa");
                }
                if (incident.getStatus().equals("wait")) {
                    medt_detailinci_status.setText("Chờ xác nhận");
                }
                Call<ElectricPole> getpole = jsonPlaceHolderApi_Data.getEPbyID(incident.getIdpole());
                getpole.enqueue(new Callback<ElectricPole>() {
                    @Override
                    public void onResponse(Call<ElectricPole> call, Response<ElectricPole> response) {
                        medt_detailinci_idpole.setText(response.body().getPole_Name());
                    }

                    @Override
                    public void onFailure(Call<ElectricPole> call, Throwable t) {

                    }
                });

                Call<Employee> getdetect = jsonPlaceHolderApi_Data.getEmpbyID(incident.getIddetect());
                getdetect.enqueue(new Callback<Employee>() {
                    @Override
                    public void onResponse(Call<Employee> call, Response<Employee> response) {
                        medt_detailinci_iddetect.setText(response.body().getName());
                    }

                    @Override
                    public void onFailure(Call<Employee> call, Throwable t) {

                    }
                });
                Call<Employee> getfixer = jsonPlaceHolderApi_Data.getEmpbyID(incident.getIdfix());
                getfixer.enqueue(new Callback<Employee>() {
                    @Override
                    public void onResponse(Call<Employee> call, Response<Employee> response) {
                        medt_detailinci_idfixer.setText(response.body().getName());
                    }

                    @Override
                    public void onFailure(Call<Employee> call, Throwable t) {

                    }
                });
            }

            @Override
            public void onFailure(Call<Incident> call, Throwable t) {

            }
        });
        mbtn_detailinci_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Chế độ Bản đồ", Toast.LENGTH_SHORT).show();
            }
        });

        mbtn_detailinci_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        mbtn_detailinci_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                medt_detailinci_name.setEnabled(false);
                medt_detailinci_des.setEnabled(false);
                medt_detailinci_date.setEnabled(false);
                medt_detailinci_level.setVisibility(View.VISIBLE);
                medt_detailinci_status.setVisibility(View.VISIBLE);
                medt_detailinci_idfixer.setVisibility(View.VISIBLE);
                msp_detailinci_level.setVisibility(View.INVISIBLE);
                msp_detailinci_status.setVisibility(View.INVISIBLE);
                msp_detailinci_idfixer.setVisibility(View.INVISIBLE);
                mbtn_detailinci_done.setVisibility(View.VISIBLE);
                mbtn_detailinci_edit.setVisibility(View.VISIBLE);
                mbtn_detailinci_save.setVisibility(View.INVISIBLE);
                mbtn_detailinci_cancel.setVisibility(View.INVISIBLE);
                String imageUrl = "http://192.168.0.169:8080/photos/byte/" + incident.getImage();
                Picasso.get().load(imageUrl).into(mpv_detailinci_image);
                medt_detailinci_name.setText(incident.getName());
                medt_detailinci_des.setText(incident.getDes());
                medt_detailinci_date.setText(incident.getDate());
                medt_detailinci_level.setText(incident.getLevel());
                if (incident.getStatus().equals("norepair")) {
                    medt_detailinci_status.setText("Chưa sửa chữa");
                }
                if (incident.getStatus().equals("repairing")) {
                    medt_detailinci_status.setText("Đang sửa chữa");
                }
                if (incident.getStatus().equals("repaired")) {
                    medt_detailinci_status.setText("Đã sửa chữa");
                }
                if (incident.getStatus().equals("wait")) {
                    medt_detailinci_status.setText("Chờ xác nhận");
                }
                Call<ElectricPole> getpole = jsonPlaceHolderApi_Data.getEPbyID(incident.getIdpole());
                getpole.enqueue(new Callback<ElectricPole>() {
                    @Override
                    public void onResponse(Call<ElectricPole> call, Response<ElectricPole> response) {
                        medt_detailinci_idpole.setText(response.body().getPole_Name());
                    }

                    @Override
                    public void onFailure(Call<ElectricPole> call, Throwable t) {

                    }
                });

                Call<Employee> getdetect = jsonPlaceHolderApi_Data.getEmpbyID(incident.getIddetect());
                getdetect.enqueue(new Callback<Employee>() {
                    @Override
                    public void onResponse(Call<Employee> call, Response<Employee> response) {
                        medt_detailinci_iddetect.setText(response.body().getName());
                    }

                    @Override
                    public void onFailure(Call<Employee> call, Throwable t) {

                    }
                });
                Call<Employee> getfixer = jsonPlaceHolderApi_Data.getEmpbyID(incident.getIdfix());
                getfixer.enqueue(new Callback<Employee>() {
                    @Override
                    public void onResponse(Call<Employee> call, Response<Employee> response) {
                        medt_detailinci_idfixer.setText(response.body().getName());
                    }

                    @Override
                    public void onFailure(Call<Employee> call, Throwable t) {

                    }
                });
            }
        });

        mbtn_detailinci_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                medt_detailinci_name.setEnabled(true);
                medt_detailinci_des.setEnabled(true);
                medt_detailinci_date.setEnabled(true);
                mbtn_detailinci_done.setVisibility(View.INVISIBLE);
                mbtn_detailinci_edit.setVisibility(View.INVISIBLE);
                mbtn_detailinci_save.setVisibility(View.VISIBLE);
                mbtn_detailinci_cancel.setVisibility(View.VISIBLE);
                medt_detailinci_level.setVisibility(View.INVISIBLE);
                medt_detailinci_status.setVisibility(View.INVISIBLE);
                medt_detailinci_idfixer.setVisibility(View.INVISIBLE);
                msp_detailinci_level.setVisibility(View.VISIBLE);
                msp_detailinci_status.setVisibility(View.VISIBLE);
                msp_detailinci_idfixer.setVisibility(View.VISIBLE);
            }
        });

        mbtn_detailinci_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (medt_detailinci_name.getText().toString().equals("") ||
                        medt_detailinci_des.getText().toString().equals("") ||
                        medt_detailinci_date.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Bạn cần nhập đầy đủ thông tin", Toast.LENGTH_LONG).show();
                } else {
                    show.setIcon(android.R.drawable.ic_dialog_alert);
                    show.setTitle("Lưu thông tin");
                    show.setMessage("Bạn có muốn lưu thông tin không ?");
                    show.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            incident.setName(medt_detailinci_name.getText().toString());
                            incident.setDes(medt_detailinci_des.getText().toString());
                            incident.setDate(medt_detailinci_date.getText().toString());


                            if (msp_detailinci_level.getSelectedItemPosition() == 0) {
                                incident.setLevel("1");
                            }

                            if (msp_detailinci_level.getSelectedItemPosition() == 1) {
                                incident.setLevel("2");
                            }
                            if (msp_detailinci_level.getSelectedItemPosition() == 2) {
                                incident.setLevel("3");
                            }
                            if (msp_detailinci_level.getSelectedItemPosition() == 3) {
                                incident.setLevel("4");
                            }

                            if (msp_detailinci_status.getSelectedItemPosition() == 0) {
                                incident.setStatus("norepair");
                            }
                            if (msp_detailinci_status.getSelectedItemPosition() == 1) {
                                incident.setStatus("repairing");
                            }

                            if (msp_detailinci_status.getSelectedItemPosition() == 2) {
                                incident.setStatus("repaired");
                            }

                            if (msp_detailinci_status.getSelectedItemPosition() == 3) {
                                incident.setStatus("wait");
                            }
                            incident.setIdfix(listfixer.get(msp_detailinci_idfixer.getSelectedItemPosition()).getId());

                            Call<String> update = jsonPlaceHolderApi.modiffyincident(incident.getId(), incident);
                            update.enqueue(new Callback<String>() {
                                @Override
                                public void onResponse(Call<String> call, Response<String> response) {
                                    Log.d("update", response.body());
                                }

                                @Override
                                public void onFailure(Call<String> call, Throwable t) {

                                }
                            });

                            Call<Incident> getbyid = jsonPlaceHolderApi.getincibyid(idinci);
                            getbyid.enqueue(new Callback<Incident>() {
                                @Override
                                public void onResponse(Call<Incident> call, Response<Incident> response) {
                                    incident = response.body();
                                    String imageUrl = "http://192.168.0.169:8080/photos/byte/" + incident.getImage();
                                    Picasso.get().load(imageUrl).into(mpv_detailinci_image);
                                    medt_detailinci_name.setText(incident.getName());
                                    medt_detailinci_des.setText(incident.getDes());
                                    medt_detailinci_date.setText(incident.getDate());
                                    medt_detailinci_level.setText(incident.getLevel());
                                    if (incident.getStatus().equals("norepair")) {
                                        medt_detailinci_status.setText("Chưa sửa chữa");
                                    }
                                    if (incident.getStatus().equals("repairing")) {
                                        medt_detailinci_status.setText("Đang sửa chữa");
                                    }
                                    if (incident.getStatus().equals("repaired")) {
                                        medt_detailinci_status.setText("Đã sửa chữa");
                                    }
                                    if (incident.getStatus().equals("wait")) {
                                        medt_detailinci_status.setText("Chờ xác nhận");
                                    }
                                    Call<ElectricPole> getpole = jsonPlaceHolderApi_Data.getEPbyID(incident.getIdpole());
                                    getpole.enqueue(new Callback<ElectricPole>() {
                                        @Override
                                        public void onResponse(Call<ElectricPole> call, Response<ElectricPole> response) {
                                            medt_detailinci_idpole.setText(response.body().getPole_Name());
                                        }

                                        @Override
                                        public void onFailure(Call<ElectricPole> call, Throwable t) {

                                        }
                                    });

                                    Call<Employee> getdetect = jsonPlaceHolderApi_Data.getEmpbyID(incident.getIddetect());
                                    getdetect.enqueue(new Callback<Employee>() {
                                        @Override
                                        public void onResponse(Call<Employee> call, Response<Employee> response) {
                                            medt_detailinci_iddetect.setText(response.body().getName());
                                        }

                                        @Override
                                        public void onFailure(Call<Employee> call, Throwable t) {

                                        }
                                    });
                                    Call<Employee> getfixer = jsonPlaceHolderApi_Data.getEmpbyID(incident.getIdfix());
                                    getfixer.enqueue(new Callback<Employee>() {
                                        @Override
                                        public void onResponse(Call<Employee> call, Response<Employee> response) {
                                            medt_detailinci_idfixer.setText(response.body().getName());
                                        }

                                        @Override
                                        public void onFailure(Call<Employee> call, Throwable t) {

                                        }
                                    });
                                }

                                @Override
                                public void onFailure(Call<Incident> call, Throwable t) {

                                }
                            });

                            medt_detailinci_name.setEnabled(false);
                            medt_detailinci_des.setEnabled(false);
                            medt_detailinci_date.setEnabled(false);
                            medt_detailinci_level.setVisibility(View.VISIBLE);
                            medt_detailinci_status.setVisibility(View.VISIBLE);
                            medt_detailinci_idfixer.setVisibility(View.VISIBLE);
                            msp_detailinci_level.setVisibility(View.INVISIBLE);
                            msp_detailinci_status.setVisibility(View.INVISIBLE);
                            msp_detailinci_idfixer.setVisibility(View.INVISIBLE);
                            mbtn_detailinci_done.setVisibility(View.VISIBLE);
                            mbtn_detailinci_edit.setVisibility(View.VISIBLE);
                            mbtn_detailinci_save.setVisibility(View.INVISIBLE);
                            mbtn_detailinci_cancel.setVisibility(View.INVISIBLE);
                        }
                    });
                    show.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            medt_detailinci_name.setEnabled(false);
                            medt_detailinci_des.setEnabled(false);
                            medt_detailinci_date.setEnabled(false);
                            medt_detailinci_level.setVisibility(View.VISIBLE);
                            medt_detailinci_status.setVisibility(View.VISIBLE);
                            medt_detailinci_idfixer.setVisibility(View.VISIBLE);
                            msp_detailinci_level.setVisibility(View.INVISIBLE);
                            msp_detailinci_status.setVisibility(View.INVISIBLE);
                            msp_detailinci_idfixer.setVisibility(View.INVISIBLE);
                            mbtn_detailinci_done.setVisibility(View.VISIBLE);
                            mbtn_detailinci_edit.setVisibility(View.VISIBLE);
                            mbtn_detailinci_save.setVisibility(View.INVISIBLE);
                            mbtn_detailinci_cancel.setVisibility(View.INVISIBLE);
                            String imageUrl = "http://192.168.0.169:8080/photos/byte/" + incident.getImage();
                            Picasso.get().load(imageUrl).into(mpv_detailinci_image);
                            medt_detailinci_name.setText(incident.getName());
                            medt_detailinci_des.setText(incident.getDes());
                            medt_detailinci_date.setText(incident.getDate());
                            medt_detailinci_level.setText(incident.getLevel());
                            if (incident.getStatus().equals("norepair")) {
                                medt_detailinci_status.setText("Chưa sửa chữa");
                            }
                            if (incident.getStatus().equals("repairing")) {
                                medt_detailinci_status.setText("Đang sửa chữa");
                            }
                            if (incident.getStatus().equals("repaired")) {
                                medt_detailinci_status.setText("Đã sửa chữa");
                            }
                            if (incident.getStatus().equals("wait")) {
                                medt_detailinci_status.setText("Chờ xác nhận");
                            }
                            Call<ElectricPole> getpole = jsonPlaceHolderApi_Data.getEPbyID(incident.getIdpole());
                            getpole.enqueue(new Callback<ElectricPole>() {
                                @Override
                                public void onResponse(Call<ElectricPole> call, Response<ElectricPole> response) {
                                    medt_detailinci_idpole.setText(response.body().getPole_Name());
                                }

                                @Override
                                public void onFailure(Call<ElectricPole> call, Throwable t) {

                                }
                            });

                            Call<Employee> getdetect = jsonPlaceHolderApi_Data.getEmpbyID(incident.getIddetect());
                            getdetect.enqueue(new Callback<Employee>() {
                                @Override
                                public void onResponse(Call<Employee> call, Response<Employee> response) {
                                    medt_detailinci_iddetect.setText(response.body().getName());
                                }

                                @Override
                                public void onFailure(Call<Employee> call, Throwable t) {

                                }
                            });
                            Call<Employee> getfixer = jsonPlaceHolderApi_Data.getEmpbyID(incident.getIdfix());
                            getfixer.enqueue(new Callback<Employee>() {
                                @Override
                                public void onResponse(Call<Employee> call, Response<Employee> response) {
                                    medt_detailinci_idfixer.setText(response.body().getName());
                                }

                                @Override
                                public void onFailure(Call<Employee> call, Throwable t) {

                                }
                            });

                        }
                    });
                    show.show();
                }
            }
        });
    }
}
