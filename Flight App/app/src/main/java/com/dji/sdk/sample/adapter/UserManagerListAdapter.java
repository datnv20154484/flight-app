package com.dji.sdk.sample.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dji.sdk.sample.R;
import com.dji.sdk.sample.model.Employee;

import java.util.ArrayList;


public class UserManagerListAdapter extends BaseAdapter {
    private Context context;
    private int layout;
    private ArrayList<Employee> listsp;

    public UserManagerListAdapter(Context context, int layout, ArrayList<Employee> listsp) {
        this.context = context;
        this.layout = layout;
        this.listsp = listsp;
    }

    @Override
    public int getCount() {
        return listsp.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {

        TextView mtv_name;
        TextView mtv_phone;
        TextView mtv_role;
        TextView mtv_mail;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        UserManagerListAdapter.ViewHolder holder;
        if (view == null) {
            holder = new UserManagerListAdapter.ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(layout, null);
            holder.mtv_name = view.findViewById(R.id.tv_lv_name);
            holder.mtv_phone = view.findViewById(R.id.tv_lv_phone);
            holder.mtv_role = view.findViewById(R.id.tv_lv_role);
            holder.mtv_mail = view.findViewById(R.id.tv_lv_mail);
            view.setTag(holder);

        } else {
            holder = (UserManagerListAdapter.ViewHolder) view.getTag();
        }
        Employee sp = listsp.get(i);
        holder.mtv_name.setText(sp.getName());
        holder.mtv_phone.setText(sp.getPhone());
        holder.mtv_role.setText(sp.getRole());
        holder.mtv_mail.setText(sp.getMail());
        return view;

    }

}
