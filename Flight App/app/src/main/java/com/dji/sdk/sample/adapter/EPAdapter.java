package com.dji.sdk.sample.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dji.sdk.sample.R;
import com.dji.sdk.sample.model.ElectricPole;

import java.util.ArrayList;

public class EPAdapter extends BaseAdapter {
    private Context context;
    private int layout;
    private ArrayList<ElectricPole> listep;

    public EPAdapter(Context context, int layout, ArrayList<ElectricPole> listsp) {
        this.context = context;
        this.layout = layout;
        this.listep = listsp;
    }

    @Override
    public int getCount() {
        return listep.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {

        TextView mtv_ep_name;
        TextView mtv_ep_latitude;
        TextView mtv_ep_longitude;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        EPAdapter.ViewHolder holder;
        if (view == null) {
            holder = new EPAdapter.ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(layout, null);
            holder.mtv_ep_name = view.findViewById(R.id.tv_ep_name);
            holder.mtv_ep_latitude = view.findViewById(R.id.tv_ep_latitude);
            holder.mtv_ep_longitude = view.findViewById(R.id.tv_ep_longitude);
            view.setTag(holder);

        } else {
            holder = (EPAdapter.ViewHolder) view.getTag();
        }
        ElectricPole sp = listep.get(i);
        holder.mtv_ep_name.setText(sp.getPole_Name());
        holder.mtv_ep_latitude.setText(sp.getPole_Latitude().toString());
        holder.mtv_ep_longitude.setText(sp.getPole_Longitude().toString());
        return view;

    }

}