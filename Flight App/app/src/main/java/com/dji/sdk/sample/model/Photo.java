package com.dji.sdk.sample.model;

import com.google.gson.annotations.SerializedName;

import org.bson.types.Binary;

public class Photo {

    @SerializedName("_id")
    private String id;
    @SerializedName("title")
    private String title;
    @SerializedName("dateCreate")
    private String dateCreate;
    @SerializedName("dateImport")
    private String dateImport;
    @SerializedName("description")
    private String description;
    @SerializedName("idpole")
    private String idpole;
    @SerializedName("iduser")
    private String iduser;
    @SerializedName("iddrone")
    private String iddrone;
    @SerializedName("crop")
    private boolean crop;
    @SerializedName("image")
    private Binary image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(String dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getDateImport() {
        return dateImport;
    }

    public void setDateImport(String dateImport) {
        this.dateImport = dateImport;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIdpole() {
        return idpole;
    }

    public void setIdpole(String idpole) {
        this.idpole = idpole;
    }

    public String getIduser() {
        return iduser;
    }

    public void setIduser(String iduser) {
        this.iduser = iduser;
    }

    public String getIddrone() {
        return iddrone;
    }

    public void setIddrone(String iddrone) {
        this.iddrone = iddrone;
    }

    public boolean isCrop() {
        return crop;
    }

    public void setCrop(boolean crop) {
        this.crop = crop;
    }

    public Binary getImage() {
        return image;
    }

    public void setImage(Binary image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Photo{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", dateCreate='" + dateCreate + '\'' +
                ", dateImport='" + dateImport + '\'' +
                ", description='" + description + '\'' +
                ", idpole='" + idpole + '\'' +
                ", iduser='" + iduser + '\'' +
                ", iddrone='" + iddrone + '\'' +
                ", crop=" + crop +
                '}';
    }
}
