package com.dji.sdk.sample.model;

import com.google.gson.annotations.SerializedName;

import org.bson.types.ObjectId;

import java.util.Date;

public class Drone {

    @SerializedName("_id")
    public String _id;
    @SerializedName("name")
    private String name;
    @SerializedName("des")
    private String des;
    @SerializedName("idman")
    private String idman;
    @SerializedName("online")
    private boolean online;
    @SerializedName("battery")
    private int battery;
    @SerializedName("dateuse")
    private Date dateuse;
    @SerializedName("status")
    private String status;
    @SerializedName("maintenancetime")
    private Date maintenancetime;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getIdman() {
        return idman;
    }

    public void setIdman(String idman) {
        this.idman = idman;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public int getBattery() {
        return battery;
    }

    public void setBattery(int battery) {
        this.battery = battery;
    }

    public Date getDateuse() {
        return dateuse;
    }

    public void setDateuse(Date dateuse) {
        this.dateuse = dateuse;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getMaintenancetime() {
        return maintenancetime;
    }

    public void setMaintenancetime(Date maintenancetime) {
        this.maintenancetime = maintenancetime;
    }

    @Override
    public String toString() {
        return "Drone{" +
                "_id=" + _id +
                ", name='" + name + '\'' +
                ", des='" + des + '\'' +
                ", idman='" + idman + '\'' +
                ", online=" + online +
                ", battery=" + battery +
                ", dateuse=" + dateuse +
                ", status='" + status + '\'' +
                ", maintenancetime=" + maintenancetime +
                '}';
    }
}
