package com.dji.sdk.sample.model;

import com.google.gson.annotations.SerializedName;

public class Incident {

    @SerializedName("_id")
    public String id;
    @SerializedName("name")
    public String name;
    @SerializedName("des")
    public String des;
    @SerializedName("date")
    public String date;
    @SerializedName("status")
    public String status;
    @SerializedName("level")
    public String level;
    @SerializedName("idfix")
    public String idfix;
    @SerializedName("iddetect")
    public String iddetect;
    @SerializedName("idpole")
    public String idpole;
    @SerializedName("image")
    public String image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getIdfix() {
        return idfix;
    }

    public void setIdfix(String idfix) {
        this.idfix = idfix;
    }

    public String getIddetect() {
        return iddetect;
    }

    public void setIddetect(String iddetect) {
        this.iddetect = iddetect;
    }

    public String getIdpole() {
        return idpole;
    }

    public void setIdpole(String idpole) {
        this.idpole = idpole;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Incident{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", des='" + des + '\'' +
                ", date='" + date + '\'' +
                ", status='" + status + '\'' +
                ", level='" + level + '\'' +
                ", idfix='" + idfix + '\'' +
                ", iddetect='" + iddetect + '\'' +
                ", idpole='" + idpole + '\'' +
                ", image='" + image + '\'' +
                '}';
    }
}
