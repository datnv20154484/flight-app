package com.dji.sdk.sample.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dji.sdk.sample.R;
import com.dji.sdk.sample.model.ElectricPole;
import com.dji.sdk.sample.model.Incident;

import java.util.ArrayList;

public class ListInciAdapter extends BaseAdapter {
    private Context context;
    private int layout;
    private ArrayList<Incident> listep;

    public ListInciAdapter(Context context, int layout, ArrayList<Incident> listsp) {
        this.context = context;
        this.layout = layout;
        this.listep = listsp;
    }

    @Override
    public int getCount() {
        return listep.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {

        TextView mtv_inci_name;
        TextView mtv_inci_des;
        TextView mtv_inci_date;
        TextView mtv_inci_status;
        TextView mtv_inci_level;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ListInciAdapter.ViewHolder holder;
        if (view == null) {
            holder = new ListInciAdapter.ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(layout, null);
            holder.mtv_inci_name = view.findViewById(R.id.iteminci_name);
            holder.mtv_inci_des = view.findViewById(R.id.iteminci_des);
            holder.mtv_inci_date = view.findViewById(R.id.iteminci_date);
            holder.mtv_inci_status = view.findViewById(R.id.iteminci_status);
            holder.mtv_inci_level = view.findViewById(R.id.iteminci_level);
            view.setTag(holder);

        } else {
            holder = (ListInciAdapter.ViewHolder) view.getTag();
        }
        Incident sp = listep.get(i);
        holder.mtv_inci_name.setText(sp.getName());
        holder.mtv_inci_des.setText(sp.getDes());
        holder.mtv_inci_date.setText(sp.getDate());
        holder.mtv_inci_level.setText(sp.getLevel());
        if(sp.getStatus().equals("norepair")){
            holder.mtv_inci_status.setText("Chưa sửa chữa");
        }
        if(sp.getStatus().equals("repairing")){
            holder.mtv_inci_status.setText("Đang sửa chữa");
        }
        if(sp.getStatus().equals("wait")){
            holder.mtv_inci_status.setText("Chờ xác nhận");
        }
        if(sp.getStatus().equals("repaired")){
            holder.mtv_inci_status.setText("Đã sửa chữa");
        }

        return view;

    }

}