package com.dji.sdk.sample.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.dji.sdk.sample.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AutoCheckInciFragment extends Fragment {

    public AutoCheckInciFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_auto_check_inci, container, false);


        return root;
    }
}
